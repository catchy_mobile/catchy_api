- Flask-principal
 - auth fixer les rôles à l'authenification, puis acces au décorateurs
- add restplus api bluprint (luis)
- add small python_API_client
-  


Done
=====

J'aurais besoin que tu m'aide à trasformer le projet tuto: https://gitlab.zettafox.com/r_d/catchy_api de façons à ce qu'il utilise

- cli à la façon de dbinit tel que décrit ici: http://flask.pocoo.org/docs/0.12/tutorial/dbinit + unittest
- mais dans le context d'un python package (avce un setup.py c'est important) Cf. http://flask.pocoo.org/docs/0.12/tutorial/packaging/#tutorial-packaging
- et bien sur en utilisant une factory like exposed here : http://flask.pocoo.org/docs/0.12/patterns/appfactories/#app-factories

must read: https://github.com/pallets/flask/issues/1847

Proposition de cible
----------------------

Target structur::



MANIFEST.in::

  graft catchy_api

setup.py::

  from setuptools import setup, find_packages

  setup(
      name='catchy_api',
      version='1.0',
      packages=find_packages(),
      include_package_data=True,
      install_requires=['click',],
      ##  http://click.pocoo.org/5/setuptools/
      entry_points="""
          [console_scripts]
          factory=yourpackage.scripts.yourscript:cli
      """,
  )


run.py::

  dev avec flask
  prod:waitres

cli.py to update_db and to run unit_test::

  import os
  import click
  from flask.cli import FlaskGroup
  from app import create_app, db

  app = create_app(os.getenv('FLASK_CONFIG') or 'default')

  @app.cli.command()
  def init_db():
      click.echo('Init the db')
      from app.models import Training, Poll
      db.create_all()
      t1 = Training('Ven13')
      p11 = Poll('q1', t1.id, question='prefered color ?')
      p12 = Poll('q2', t1.id, question='prefered song ?')
      with app.app_context():
          db.session.add(t1)
          db.session.add(p11)
          db.session.add(p12)
          db.session.commit()

  if __name__ == '__main__':
      init_db()

