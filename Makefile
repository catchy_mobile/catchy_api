clean:
	@rm -fr ./catchy_api/__pycache__
	@rm -fr ./catchy_api/api/__pycache__
	@rm -fr flask-inspinia
	@rm -fr ./catchy_api/fixtures/__pycache__
	@rm -fr build
	@rm -f ghostdriver.log
	@rm -fr dist
	@rm -fr catchy_api.egg-info
	@echo '  => Clean done'

db_init: dev_install
	catchy_apicli drop_tables && catchy_apicli create_tables \
		&& catchy_apicli load_fixtures

dev_install:
# use with FLASK_DEBUG=1 catchy_apicli drop_tables; catchy_apicli create_tables;
#                        catchy_apicli load_fixtures; catchy_apicli run;
# to get server autorestart when code is changed
	@pip3 install -e .

dev_run: dev_install
	@cd /tmp/; FLASK_DEBUG=1 catchy_apicli run

install:
	@pip3 install . -U

run: install
	@cd /tmp/; catchy_apicli run

wheel: clean
	@python3 setup.py bdist_wheel
	@printf '\n => wheel ready %s\n\n' $$(find . -name '*.whl')

python_count_lines:
	@find ./catchy_api -name '*.py' -exec  wc -l {} \; | sort -n| awk \
        '{printf "%4s %s\n", $$1, $$2}{s+=$$0}END{print s}'
	@echo ''
	@find ./tests -name '*.py' -exec  wc -l {} \; | sort -n| awk \
        '{printf "%4s %s\n", $$1, $$2}{s+=$$0}END{print s}'
	@echo ''

check_code:
	@flake8 scripts/* catchy_api/*.py catchy_api/api/*.py tests/*.py

test:
	@cy_init_index -f
	@cy_inject_sample_data
	@cd tests; python3 -m unittest discover
