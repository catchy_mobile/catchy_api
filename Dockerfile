FROM python:3.6

ENV PYTHONUNBUFFERED 1

ENV FLASK_APP=catchy_api/start.py
RUN mkdir /code
WORKDIR /code
# RUN mkdir /code/flask-inspinia
# ADD ./flask-inspinia/ /code/flask-inspinia/
ADD ./requirements.txt /code/requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
ADD . /code/
RUN pip install -e .
