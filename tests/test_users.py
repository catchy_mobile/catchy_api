import logging
import unittest
import json
from datetime import datetime
import os
import tempfile

from catchy_api.app import create_app
from catchy_api.fixtures.install import inject_users


class UsersTest(unittest.TestCase):
    def setUp(self):
        self.db_fd, self.dbname = tempfile.mkstemp()
        self.app = create_app(DATABASE="sqlite:///" + self.dbname)
        self.app.testing = True
        with self.app.app_context():
            from catchy_api.models import db
            db.create_all()
            inject_users()

        # tokens have limited life span
        with self.app.test_client() as c:
            r = c.post("/api/1/user/login/",
                       data=json.dumps({"login": "admin@domaine.tld",
                                        "password": "secret"}),
                       content_type='application/json')
            data = json.loads(r.get_data(as_text=True))
            self.auth = {'Authorization': 'jwt ' + data['access_token']}

        log = logging.getLogger('politis-frontend')
        log.setLevel(logging.WARNING)

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(self.dbname)

    def test_login(self):
        with self.app.test_client() as c:
            r = c.post("/api/1/user/login/",
                       data=json.dumps({"login": "admin@domaine.tld",
                                        "password": "foo"}),
                       content_type='application/json')
            self.assertEqual(r.status_code, 401)

            r = c.post("/api/1/user/login/",
                       data=json.dumps({"login": "bar@domaine.tld",
                                        "password": "foo"}),
                       content_type='application/json')
            self.assertEqual(r.status_code, 401)

    def test_users(self):
        with self.app.test_client() as c:
            data = json.loads(c.get("/api/1/users/", headers=self.auth)
                              .get_data(as_text="True"))
            self.assertEqual(len(data), 2)
            self.assertListEqual([u["screenname"] for u in data],
                                 ["Admin", "John Doe"])
            self.assertListEqual([u["role"] for u in data],
                                 ["administrator", "student"])
            self.assertListEqual([u["active"] for u in data], [True, True])
            self.assertListEqual([u["login"] for u in data],
                                 ["admin@domaine.tld", "john@doe.tld"])

    def test_signup_delete(self):
        new = {"login": "example2@zettafox.com",
               "password": "mymdp2",
               "screenname": "babyfox2"}
        with self.app.test_client() as c:
            # Create a user
            r = c.post("/api/1/user/signup/", data=json.dumps(new),
                       content_type="application/json",
                       headers=self.auth)
            self.assertEqual(r.status_code, 200)
            data = json.loads(r.get_data(as_text="True"))
            uid = data['id']
            self.assertEqual(data['login'], new['login'])
            self.assertEqual(data['screenname'], new['screenname'])
            self.assertEqual(data['role'], "administrator")
            self.assertTrue(data['active'])
            creation_diff_time = (datetime.strptime(data['created_at'],
                                                    "%Y-%m-%d %H:%M:%S.%f")
                                  - datetime.now())
            self.assertTrue(creation_diff_time.total_seconds() < 60)

            # Check if we can login
            r = c.post("/api/1/user/login/",
                       data=json.dumps({"login": new["login"],
                                        "password": new["password"]}),
                       content_type='application/json')
            self.assertEqual(r.status_code, 200)

            # Delete user
            r = c.delete("/api/1/users/{0:d}/".format(uid), headers=self.auth)
            self.assertEqual(r.status_code, 200)

            # Check that it is really deleted
            data = json.loads(c.get("/api/1/users/", headers=self.auth)
                              .get_data(as_text="True"))
            self.assertNotIn(uid, [u['id'] for u in data])
