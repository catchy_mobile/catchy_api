import logging
import unittest
import json
import os
import tempfile

from catchy_api.app import create_app
from catchy_api.fixtures.install import inject_users


class CommentsTest(unittest.TestCase):
    def setUp(self):
        self.db_fd, self.dbname = tempfile.mkstemp()
        self.app = create_app(DATABASE="sqlite:///" + self.dbname)
        self.app.testing = True
        with self.app.app_context():
            from catchy_api.models import db
            db.create_all()
            inject_users()

        # tokens have limited life span
        with self.app.test_client() as c:
            r = c.post("/api/1/user/login/",
                       data=json.dumps({"login": "admin@domaine.tld",
                                        "password": "secret"}),
                       content_type='application/json')
            data = json.loads(r.get_data(as_text=True))
            self.auth = {'Authorization': 'jwt ' + data['access_token']}

        log = logging.getLogger('politis-frontend')
        log.setLevel(logging.WARNING)

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(self.dbname)

    def test_get_comments(self):
        with self.app.test_client() as c:
            r = c.get("/api/1/comments/", headers=self.auth)
            data = json.loads(r.get_data(as_text="True"))
            self.assertEqual(len(data), 35)
