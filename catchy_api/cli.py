import click
from flask.cli import FlaskGroup
# from flask import current_app as app
from catchy_api.app import create_app


# which cli decorator to give an optionnal config.py abspath ?
def create_instance(info):
    return create_app()


@click.group(cls=FlaskGroup, create_app=create_instance)
def cli():
    """This is a management script for catchy_api."""
    pass


@cli.command()
def create_tables():
    """Create Database Tables"""
    from catchy_api.models import db
    db.create_all()


@cli.command()
def drop_tables():
    """Drop Database Tables"""
    from catchy_api.models import db
    db.reflect()
    db.drop_all()


@cli.command()
def load_fixtures():
    """Create initial data"""
    from catchy_api.fixtures.install import inject_users
    inject_users()


@cli.command()
def reset_fixtures():
    """Reset Database and create initial data"""
    drop_tables()
    create_tables()
    load_fixtures()
