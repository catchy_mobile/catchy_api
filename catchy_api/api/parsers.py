from flask_restplus import reqparse, inputs

# User relared parsing
signup = reqparse.RequestParser()
signup.add_argument('login', type=str, location='json', required=True,
                    help='Email')
signup.add_argument('password', type=str, location='json', required=True,
                    help='Password')
signup.add_argument('screenname', type=str, location='json', required=True,
                    help='screenname')

put_user = reqparse.RequestParser()
put_user.add_argument('login', type=str, location='json', required=True,
                      help='login')
put_user.add_argument('password', type=str, location='json', required=True,
                      help='Password')
put_user.add_argument('role', type=str, location='json', required=True,
                      help='Role')
put_user.add_argument('active', type=bool, location='json', default=True,
                      help='Active')
# put_user.add_argument('confirmed_at', type=inputs.datetime_from_iso8601,
#                       location='json', required=True, help='Confirmed at')
put_user.add_argument('created_at', type=inputs.datetime_from_iso8601,
                      location='json', help='Created at')

users = reqparse.RequestParser()
users.add_argument('page', type=int, default=1, help='Page number')
users.add_argument('per_page', type=int, default=10, help='Items per page')


put_app_user = reqparse.RequestParser()
put_app_user.add_argument('login', type=str, location='json', required=True,
                          help='Email')
put_app_user.add_argument('phone', type=str, location='json', required=True,
                          help='Phone')
put_app_user.add_argument('first_name', type=str, location='json',
                          required=True, help='First Name')
put_app_user.add_argument('last_name', type=str, location='json',
                          required=True, help='Last Name')
put_app_user.add_argument('password', type=str, location='json', required=True,
                          help='Password')
put_app_user.add_argument('udid', type=str, location='json', required=True,
                          help='UDID')
put_app_user.add_argument('active', type=bool, location='json', required=True,
                          help='Active')
