from flask_restplus import fields, Model

article = Model('ArticleFields', {
    'num_dates': fields.Integer,
    'page_per_day': fields.Integer,
})

poll = Model('PollFields', {
    'id': fields.Integer,
    'name': fields.String,
    'created_at': fields.DateTime(),
})

choice = Model('ChoiceFields', {
    'id': fields.Integer,
    'label': fields.String,
    'question_id': fields.Integer,
    'created_at': fields.DateTime(),
})

question = Model('QuestionFields', {
    'id':
    fields.Integer,
    'label':
    fields.String,
    'input_type':
    fields.String,
    'position':
    fields.Integer,
    'image':
    fields.String,
    'poll_id':
    fields.Integer,
    'created_at':
    fields.DateTime(),
    'choices':
    fields.List(fields.Nested(choice), attribute=lambda x: x.choices.all()),
})

reply = Model('ReplyFields', {
    'id': fields.Integer,
    'user_id': fields.Integer,
    'question_id': fields.Integer,
    'choice_id': fields.Integer,
    'created_at': fields.DateTime(),
})


user = Model('UserFields', {
    'id': fields.Integer,
    'login': fields.String,
    'screenname': fields.String,
    'role': fields.String,
    'active': fields.Boolean,
    'created_at': fields.String,
})
