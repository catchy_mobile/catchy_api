from flask_jwt import current_identity
from flask_restplus import abort

from functools import wraps


def require(role='administrator'):
    def dec(wrapped):
        @wraps(wrapped)
        def inner(*args, **kwargs):
            if not current_identity.is_anonymous:
                if current_identity.has_role(role) and \
                        current_identity.is_active:
                    return wrapped(*args, **kwargs)
            abort(403, 'Not allowed')
        return inner
    return dec
