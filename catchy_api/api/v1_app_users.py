from flask import request
# from flask_jwt import jwt_required
from flask_restplus import Namespace, Resource

# from catchy_api.api import auth

from catchy_pkg.query import get_type_data
from catchy_pkg.inject import inject_users
from catchy_pkg.es_mapping import cnx

api = Namespace('App_users', path='/app_users',
                description="mobile app users data")


@api.route('/', methods=["GET", "POST"])
class Users(Resource):
    # @jwt_required()
    # @auth.require('administrator')
    def get(self, index='catchy'):
        """
        curl -H "Authorization: $MYTOKEN" "localhost:5000/api/1/app_users/"
        """
        return get_type_data(cnx, 'users', index='catchy')

    def post(self, index='catchy'):
        """
        curl -H "Authorization: $MYTOKEN"
        -H "Content-type: Application/json"
        "localhost:5000/api/1/app_users/"
        -d '[{"postcode": 92150}]'
        """
        print("HEEEEEEEEEERE: {}".format(request.json))
        inject_users(cnx, request.json, index)
