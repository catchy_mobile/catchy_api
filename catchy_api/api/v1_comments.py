from flask import request
# from flask_jwt import jwt_required
from flask_restplus import Namespace, Resource

# from catchy_api.api import auth

from catchy_pkg.query import get_type_data
from catchy_pkg.inject import inject_comments
from catchy_pkg.es_mapping import cnx

api = Namespace('Comments', path='/comments', description="Comments data")


@api.route('/')
class Comments(Resource):
    # @jwt_required()
    # @auth.require('administrator')
    def get(self, index='catchy'):
        """
        curl -H "Authorization: $MYTOKEN" "localhost:5000/api/1/comments/"
        """
        return get_type_data(cnx, 'comments', index='catchy')

    def post(self, index='catchy'):
        """
        curl -XPOST -H "Content-type: Application/json"
        "localhost:5000/api/1/comments/"
        -d '[{"content": "good"}, {"content": "bad"}]'
        """
        print("DATA : {}".format(request.json))
        inject_comments(cnx, request.json, index)
