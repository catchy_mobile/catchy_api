from flask import request
# from flask_jwt import jwt_required
from flask_restplus import Namespace, Resource

# from catchy_api.api import auth

from catchy_pkg.query import get_type_data
from catchy_pkg.inject import inject_photos_metadata
from catchy_pkg.es_mapping import cnx

api = Namespace('Photos', path='/photos', description="Photos metadata")


@api.route('/')
class Photos(Resource):
    # @jwt_required()
    # @auth.require('administrator')
    def get(self, index='catchy'):
        """
        curl -H "Authorization: $MYTOKEN" "localhost:5000/api/1/photos/"
        """
        return get_type_data(cnx, 'photos', index='catchy')

    def post(self, index='catchy'):
        """
        curl -XPOST -H "Content-type: Application/json"
        "localhost:5000/api/1/photos/"
        -d '[{"city": "Marseille"}, {"city": "Lilles"}]'
        """
        print("DATA : {}".format(request.json))
        inject_photos_metadata(cnx, request.json, index)
