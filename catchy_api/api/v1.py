from flask import Blueprint
from flask_restplus import Resource, Api

from catchy_api.api import models
from .v1_user import api as v1_user
from .v1_comments import api as v1_comments
from .v1_app_users import api as v1_app_users
from .v1_photos import api as v1_photos

v1 = Blueprint('api', __name__)

api = Api(v1,
          validate=True,
          version='1.0',
          title='catchy_api',
          description='',
          doc='/documentation/',
          default='General',
          default_label='General Operations')

api.models[models.user.name] = models.user

api.add_namespace(v1_user)
api.add_namespace(v1_comments)
api.add_namespace(v1_app_users)
api.add_namespace(v1_photos)


@api.route('/')
class Version(Resource):
    def get(self):
        return {
            'version': '1.0.0'
        }
