from flask_jwt import jwt_required, JWTError, _default_auth_request_handler
from flask_jwt import current_identity
from flask_restplus import Namespace, Resource, abort

from catchy_api.api import parsers, models, auth
from catchy_api.models import db, User
from catchy_api.fixtures.install import inject_users

api = Namespace('User', path='/user')


@api.route('s/init/')
class Init(Resource):
    def post(self):
        """
        Initialize postgreSQL database with appropriate columns
        """
        db.create_all()
        inject_users()
        print('Table "users" successfully created')


@api.route('s/')
class Users(Resource):
    @api.expect(parsers.users)
    @jwt_required()
    @auth.require('administrator')
    @api.marshal_with(models.user, as_list=True)
    def get(self):
        """
        curl -H "Authorization: $MYTOKEN" "http://127.0.0.1:5000/api/1/users/"
        """
        args = parsers.users.parse_args()
        q = User.query
        r = q.paginate(page=args['page'], per_page=args['per_page'])
        return r.items, 200, {'X-Pagination-Page': args['page'],
                              'X-Pagination-Per_Page': args['per_page'],
                              'X-Pagination-Has_Next': r.has_next,
                              'X-Pagination-Has_Prev': r.has_prev,
                              'X-Pagination-Pages': r.pages,
                              'X-Pagination-Total': r.total}


@api.errorhandler(JWTError)
def handle_custom_exception(error):
    """Return a custom message and 400 status code"""
    return {'message': error.message}, 400


@api.route('/signup/')
class Signup(Resource):
    @api.expect(parsers.signup)
    @api.marshal_with(models.user)
    def post(self):
        """
        curl -H "Content-Type: application/json" -XPOST -d
        '{"login":"example2@zettafox.com", "password":"mymdp2",
        "screenname":"babyfox2"}' "http://localhost:5000/api/1/user/signup/"
        """
        args = parsers.signup.parse_args()
        user_data = {
            "login": args.get('login'),
            "password": args.get('password'),
            "screenname": args.get('screenname'),
            "active": True
        }
        u = User(**user_data)
        u.set_password(args['password'])

        db.session.add(u)
        db.session.commit()
        return u


@api.route('/login/')
class Login(Resource):
    @api.expect(parsers.signup)
    def post(self):
        """
        $ temp=`curl -H "Content-Type: application/json" -XPOST -d
        '{"login":"example@zettafox.com", "password":"mymdp"}'
        "http://localhost:5000/api/1/user/login/" | jq '.[]'`
        $ export MYTOKEN="jwt ${temp:1: -1}"
        """
        return _default_auth_request_handler()


@api.route('/logout/')
class Logout(Resource):
    @jwt_required()
    @api.marshal_with(models.user)
    def post(self):
        u = current_identity
        # TODO: do sth with that user
        return u


@api.route('s/<user_id>/')
class UserDetail(Resource):
    @jwt_required()
    @api.marshal_with(models.user)
    def get(self, user_id):
        """
        curl -H "Authorization: $MYTOKEN" "localhost:5000/api/1/users/1/"
        """
        u = User.query.get(user_id)
        if not u:
            abort(404, 'User not found')
        # if not current_identity.has_object_access(u):
        #     abort(404, 'User not found')
        return u

    @jwt_required()
    @auth.require('administrator')
    def delete(self, user_id):
        """
        $ curl -H "Authorization: $MYTOKEN"
          -XDELETE "localhost:5000/api/1/users/4/"
        """
        u = User.query.get(user_id)
        if not u:
            abort(404, 'User not found')
        if not current_identity.has_object_access(u):
            abort(404, 'User not found')
        # u.delete()
        db.session.delete(u)
        db.session.commit()
        return {'message': 'User deleted'}, 200

    @jwt_required()
    @auth.require('administrator')
    @api.expect(parsers.put_user)
    @api.marshal_with(models.user)
    def put(self, user_id):
        u = User.query.get(user_id)
        if not u:
            abort(404, 'User not found')
        if not current_identity.has_object_access(u):
            abort(404, 'User not found')

        args = parsers.put_user.parse_args()
        for field_name, value in args.items():
            if field_name == 'password':
                u.set_password(value)
            else:
                if value is not None:
                    setattr(u, field_name, value)

        db.session.commit()

        return u
