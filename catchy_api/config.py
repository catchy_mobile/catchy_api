import os
from datetime import timedelta

MINUTE = 60
HOUR = 60 * MINUTE
DAY = 24 * HOUR

here = os.path.abspath(os.path.dirname(__file__))
BOOTSTRAP_SERVE_LOCAL = True  # This turns file serving static
SQLALCHEMY_DATABASE_URI = "sqlite:////tmp/catchy_api.db"
SQLALCHEMY_TRACK_MODIFICATIONS = False
SECRET_KEY = 'some_random_key'
UPLOAD_FOLDER = os.path.join(here, 'static/uploads')
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

JWT_AUTH_USERNAME_KEY = 'login'
JWT_AUTH_PASSWORD_KEY = 'password'
JWT_EXPIRATION_DELTA = timedelta(seconds=DAY)
