$(function() {

  $('#add-new-choice').on('click', function(e) {
    e.preventDefault();
    var $choice = $('\
      <div class="form-group">\
        <div class="row">\
          <div class="col-sm-10">\
            <input value="" name="choices" type="text" class="form-control" id="label" placeholder="Choice label" required>\
          </div>\
          <div class="col-sm-2">\
            <a href="#" class="btn btn-sm btn-danger delete-choice">\
              <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>\
            </a>\
          </div>\
        </div>\
      </div>\
    ').appendTo('#choices-group');

    $('.delete-choice', $choice).on('click', function(e) {
      $(this).parent().parent().parent().remove();
    });

    $choice.focus();

  })
})
