from os.path import abspath, dirname, join
from flask import Flask
# from flask import g
from flask_cors import CORS
from flask_jwt import JWT

from catchy_api.models import db, User


def create_app(config=None, DATABASE=None):
    app = Flask(__name__)

    if config is None:
        config = join(abspath(dirname(__file__)), 'config.py')
    app.config.from_pyfile(config)

    # We want to inject temporary database for unittests
    if DATABASE is not None:
        app.config["SQLALCHEMY_DATABASE_URI"] = DATABASE

    db.init_app(app)
    # Migrate is from flask-migrate to change the database models
    # Migrate(app, db)

    def authenticate(email, password):
        u = User.authenticate(email, password)
        if u and u.is_active is True:
            return u

    def identity(payload):
        user_id = payload['identity']
        u = User.query.get(user_id)
        if u:
            return u

    configured_jwt = JWT(app, authenticate, identity)
    app.jwt = configured_jwt

    cors = CORS(resources={r'/api/*': {"origins": "*"},
                           r'/auth*': {"origins": "*"}})
    cors.init_app(app)

    from catchy_api.api.v1 import v1 as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/api/1')

    # Load a DataFrame from catchy
    from catchy_pkg.data import dataset
    app.data = dataset('data.csv.gz')

    @app.route('/')
    def default():
        return 'Welcome'

    return app
