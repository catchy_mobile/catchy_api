import os
import json
from catchy_api.models import db, User

HERE = os.path.abspath(os.path.dirname(__file__))


def get_data():
    with open(os.path.join(HERE, 'data.json')) as data_file:
        data = json.load(data_file)
    return data

def inject_users():
    bulk = []
    data = get_data()

    for user in data["users"]:
        u = User(**user)
        u.set_password(user['password'])
        bulk.append(u)

    db.session.bulk_save_objects(bulk)
    db.session.commit()
