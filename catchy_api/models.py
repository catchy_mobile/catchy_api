from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash

from flask_sqlalchemy import SQLAlchemy
import flask_login as flask_login

db = SQLAlchemy()


class User(db.Model, flask_login.UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    screenname = db.Column(db.Unicode(255))
    login = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean(), index=True)
    role = db.Column(db.String(25), index=True, default='administrator')
    created_at = db.Column(
        db.DateTime, nullable=False, default=datetime.today())

    ROLES = {
        'administrator': {
            'label': 'Administrator'
        },
        'student': {
            'label': 'Student'
        }
    }

    @classmethod
    def authenticate(cls, login, password):
        if login and password:
            u = cls.by_login(login)
            if u and u.active is True:
                if u.check_password(password) is True:
                    return u
        return None

    @classmethod
    def by_login(cls, login):
        return cls.query.filter(cls.login == login).first()

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        if self.password and password:
            return check_password_hash(self.password, password)

    @property
    def is_active(self):
        return self.active

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def has_role(self, role):
        if len(role) > 2:
            if self.role == 'administrator':
                return True

            if self.role == role:
                return True

        return False

    def has_object_access(self, obj):
        obj_type = type(obj)

        if obj_type == type(self):
            return self.has_role('administrator') or obj.id == self.id

    def __repr__(self):
        return '<User login="%s">' % self.login
