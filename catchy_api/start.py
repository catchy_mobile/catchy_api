#!python
"""
This catchy_api/start.py used by:
  Dockerfile:ENV FLASK_APP=catchy_api/start.py

To run flask with this start.py use:
  $ cd ~/catchy_api
  $ FLASK_APP=catchy_api/start.py flask run --host=0.0.0.0

To run flask from a virtualenv use:
  $ source ~/venv3/bin/activate
  $ cd /tmp              # so we can run it from anyware
  $ catchy_apicli run           # this run catchy_api/cli.py entry_points
                             # define in setup.py
"""

import click
from waitress import serve

from catchy_api.app import create_app


app = create_app()


@app.cli.command()
@click.option('--port', default=5000, help='Application server port')
@click.option('--host', default='0.0.0.0', help='Application server host')
def waitress(port, host):
    serve(app, host=host, port=port)


@app.cli.command()
def create_tables():
    """Create Database Tables"""
    from catchy_api.models import db
    db.create_all()


@app.cli.command()
def drop_tables():
    """Drop Database Tables"""
    from catchy_api.models import db
    db.reflect()
    db.drop_all()


@app.cli.command()
def load_fixtures():
    """Create initial data"""
    from catchy_api.fixtures.install import inject_users
    inject_users()


@app.cli.command()
def reset_fixtures():
    """Reset Database and create initial data"""
    drop_tables()
    create_tables()
    load_fixtures()
