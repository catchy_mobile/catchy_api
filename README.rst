|MasterBuild|\ \_ |MasterCoverage|\ \_ |LicenseMIT|\ \_

.. |MasterBuild| image:: https://gitlab.zettafox.com/2017/catchy/badges/master/build.svg
.. |MasterCoverage| image:: https://gitlab.zettafox.com/2017/catchy/badges/master/coverage.svg?job=coverage
.. |LicenseMIT| image:: https://img.shields.io/badge/License-MIT-brightgreen.svg

.. contents:: Catchy API

Archi
=====
- Ressources
- Endpoints
- Method (Get/Post/Put/Delete)
- Persistancy_layer ES or postgres|mariadb
- CI

- Menu
  - About | Montrer | Talk | Edit_profile
- About/Help_screen:
  - Citoyen-Ville 
  - Montrer/localiser/signaler: nid de poule, paneaux cassé, signalisation, vendalisme, lampadaire
  - Parler/Dire/xxx
- Montrer_screen  
  - photo_ready_to_shoot
  - select_categorie nid de poule|paneaux cassé| ...|other
  - hidden_GPS_localisation
- Talk_screen  
  - GPS_powered_town_detection
     - alternative auto-completion_town_name
  - alternative text_edit_area to write
     - microphone_icon to talk
- Edit_profile_screen: email, tel, Nickname, code_postal (pre-remplir), hiden_phone_id


Data analysis API
=================

-  Project: Catchy
-  Description: Skeleton for data science & machine learning projects
-  Data analysis project: project that provides functionality for API

Please document the project the best you can.

Development installation
========================

The use:

.. code:: shell

  make db_init
  make dev_install
  make dev_run

to initiate database, install your api library and run the flask
server locally. It will automatically restart whenever you make
any changes.

Since you will work heavily with json, you may find this tool
for parsing json from command line useful:

.. code:: shell

  sudo apt-get install jq

Getting authentication token
----------------------------

Most parts of the API require you to provide an authentication token.
We use `JWT`_ for that. You get the token in response to login:

.. code:: shell

  temp=`curl -H "Content-Type: application/json" -XPOST -d '{"login":"admin@domaine.tld", "password":"secret"}' "http://localhost:5000/api/1/user/login/" | jq '.[]'`
  export MYTOKEN="jwt ${temp:1: -1}"

Now, you can use the token in API request:

.. code:: shell

  curl -H "Authorization: $MYTOKEN" "localhost:5000/api/1/test/"

Note that the tokens expire after some time. So don't hard-code them anywhere.

.. _`JWT`: https://jwt.io/

Database
--------

By default, API uses the sqlite database. The ``db_init`` target
of ``Makefile`` does:

.. code:: shell

  catchy_apicli drop_tables    # Drop Tables
  catchy_apicli create_tables  # Init DB
  catchy_apicli load_fixtures  # Load Fixtures


Use native sqlite3 cli to inspect the database when needed:

.. code::

  $ sqlite3 -column -header
  SQLite version 3.14.1 2016-08-11 18:53:32
  Enter ".help" for usage hints.
  sqlite> .open "/tmp/my_app.db"
  sqlite> .databases
  seq  name             file
  0    main             /tmp/my_app.db
  sqlite> .table
  sqlite> .schema
  sqlite> select * from poll;
  ...

or the GUI:

.. code:: shell
          
  # http://sqlitebrowser.org/
  sudo add-apt-repository -y ppa:linuxgndu/sqlitebrowser
  sudo apt-get update
  sudo apt-get install sqlitebrowser
  sqlitebrowser /tmp/my_app.db
