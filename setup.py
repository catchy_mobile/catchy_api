from setuptools import setup, find_packages

requirements = """
click
dominate>=2.3.1
Flask-Bootstrap >= 3.3
Flask-Cors==3.0.2
Flask-jwt==0.3.2
Flask-login==0.4.0
flask-restplus==0.10.1
Flask-SQLAlchemy==2.1
Flask==0.11.1
psycopg2
redis
Requests
selenium
setuptools_scm
visitor
gunicorn
python-gitlab
waitress
"""

setup(
    name='catchy_api',
    setup_requires=['setuptools_scm'],
    use_scm_version=True,
    author='Catchy',
    author_email='louiscassedanne@gmail.com',
    install_requires=requirements,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    # all functions @cli.command() decoreated in catchy_api/cli.py
    # will be scripts
    entry_points={'console_scripts':
                  ['catchy_apicli = catchy_api.cli:cli']}
)
